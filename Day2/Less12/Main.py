result = 0

inputFile = open( "d.in", "r" )
data = inputFile.readline()

isDef = False
isWord = False

for i in range(len(data)):
    if data[i].isalpha():
        if not isWord :
            result += 1
            isWord = True
    else:
        if data[i] != "-" :
            isWord = False

outputFile = open( "d.out", "w" )

outputFile.write( str( result ) )
outputFile.close()