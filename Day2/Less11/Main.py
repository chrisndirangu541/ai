inputFile = open("input.txt", "r")

result = {"1": 0, 
          "2": 0, 
          "3": 0, 
          "4": 0, 
          "5": 0,
          "6": 0,
          "7": 0,
          "8": 0,
          "9": 0,
          "0": 0}

count = int( inputFile.readline() )

for i in range(count):
    result[ inputFile.readline()[0] ] += 1

outputFile = open("output.txt", "w")
resultStr = ""

for i in result:
    resultStr += str( result[i] )
    resultStr += " "

outputFile.write( resultStr[:len(resultStr) - 1] )