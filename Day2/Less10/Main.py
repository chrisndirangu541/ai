import re

regex = r"\w+"

result = 0

for match in re.finditer(regex, input()):
    result += 1

print( result )
