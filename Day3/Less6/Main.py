import numpy as np

size = int( input() )

data = []

for i in range( size ):
    rawData = input()
    rawData = rawData.split(" ")
    rawData = list( filter(lambda x: x!="", rawData) )
    data += rawData

mat = np.array( data, int )

mat.shape = ( size, size )

mat = mat.transpose()

for i in mat:
    result = ""
    for t in i:
        result += str( t )
        result += " "
    print( result )

