data:str = input()

pos = int( input() ) - 1

data = data.split(" ")

data = list( filter(lambda x: x != "", data) )

if( pos >= len(data) ):
    print( data[len(data) - 1] )
else:
    print( data[pos] )
