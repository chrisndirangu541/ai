from functools import reduce

data:str = input()

data = data.split(" ")

data = list( map( int, filter(lambda x: x != "", data ) ) )

result = reduce(lambda a, b: abs(a) + abs(b), data)

print( result )


