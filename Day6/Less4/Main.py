class ShiftableList(list):
    def __lshift__(self, num):
        if type(num) != int:
            raise TypeError
        return self[num%len(self):] + self[:num%len(self)]

    def __rshift__(self, num):
        return self << -num