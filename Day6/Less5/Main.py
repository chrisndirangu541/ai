import numpy as np
import random

class Chromosome:
    # Chromosome class constructor
    # Actual functionality is to set up an array called genes.
    # If boolean flag fillGenes is set to True, genes must be
    # filled with random values between 0 and 1, otherwise
    # it must be filled with 0.
    # Length of array genes must be equal to length
    # constructor parameter.
    # Also initializes local variable mutationRate
    # with corresponding parameter.
    def __init__(self, length, mutationRate, fillGenes=False):
        self.mutationRate = mutationRate
        self.length = length
        if fillGenes:
            self.genes = np.random.random_sample(length)
        else:
            self.genes = np.zeros(length)

    # Creates two offspring children using a single crossover point.
    # The basic idea is to first pick a random position, create two
    # children and then swap their genes starting from the randomly
    # picked position point.
    # Children genes must be different from both of parents.
    #
    # Returning type: (Chromosome, Chromosome)
    def Crossover(self, another):
        resultOne = Chromosome(another.length, another.mutationRate)
        resultOne.genes = np.copy(self.genes)

        resultTwo = Chromosome(another.length, another.mutationRate)
        resultTwo.genes = np.copy(another.genes)

        pos = random.randint(0, self.length - 1)
        while resultOne.genes[pos] == resultTwo.genes[pos]:
            pos = random.randint(0, self.length - 1)
        
        transVal = resultOne.genes[pos]
        resultOne.genes[pos] = resultTwo.genes[pos]
        resultTwo.genes[pos] = transVal

        return (resultOne, resultTwo)


    # Mutates the chromosome genes in place by randomly switching them
    # depending on mutationRate. More precisely, mutation
    # of i-th gene happens with probability of mutationRate.
    def Mutate(self):
        for i in range(self.length):
            randomValue = random.uniform(0.0, 1.0)
            if randomValue <= self.mutationRate:
                self.genes[i] = random.uniform(0.0, 1.0)
