result = ""
data = input()

#сложное или нет
if( data.find(",") != -1 ):
    result += "сложное "

#тип
if( data.find("!") != -1 ):
    if( data.find("?") != -1 ):
        result += "вопросительно-восклицательное"
    else:
        result += "восклицательное"
else:
    if( data.find("?") != -1 ):
        result += "вопросительное"
    else:
        result += "повествовательное"

print( result )