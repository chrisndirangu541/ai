data = int( input() )
result = ""

if( ( data % 3 ) == 0 ):
    result += "Fizz"

if( ( data % 5 ) == 0 ):
    result += "Buzz"

print(result)