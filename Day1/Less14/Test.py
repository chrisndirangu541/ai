import Main

testValues = [[10, 5, 18, "N"],
              [1, 8, 18, "S"]]

for i in testValues:
    k = Main.Count(i[0], i[1], i[2])
    if( k != i[3] ):
        print("Error test: %s answer %s" % (i, k) )
