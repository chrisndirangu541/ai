import Main

testValues = [[28, 31, 0],
              [30, 31, 60],
              [9, 1, 0],
              [1, 60, 60],
              [1, 9, 1],
              [3, 14, 16],
              [14, 3, 16],
              [3, 58, 60],
              [8, 51, 0],
              [54, 55, 108]]

for i in testValues:
    k = Main.CountPage( i[0], i[1])
    if( k != i[2] ):
        print( "Error test: %s answer %s" % (i, k) )
