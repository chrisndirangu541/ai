
def CountPage(startH:int, startM:int,
              H:int, M:int):
    m = startM + M
    resultM = m % 60
    resultH = ( int( m / 60 ) + H + startH ) % 24
    return [resultH , resultM]

inputFile = open("input.txt", "r")
outputFile = open("output.txt", "w")

startRawData:str = inputFile.readline()
sizeRawData:str = inputFile.readline()

startData = startRawData.split(" ")
sizeData = sizeRawData.split(" ")

startData = list(filter(lambda a: a != "", startData))
sizeData = list(filter(lambda a: a != "", sizeData))

result = CountPage( int( startData[0] ), 
                    int( startData[1] ),
                    int( sizeData[0] ), 
                    int( sizeData[1] ) )

outputFile.write( "%i %i" % ( result[0], result[1] ) )

