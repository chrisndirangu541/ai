inputFile = open("input.txt")
count = int(inputFile.readline())

reg = {}

for i in range(count):
    data = inputFile.readline()
    data = data[:len(data)-1]
    data = data.split("\t")
    if data[0] in reg.keys():
        reg[data[0]].append(data[1])
    else:
        reg[data[0]] = [data[1]]

outputFile = open("output.txt", "w")

keys = list(reg.keys())
keys.sort()

for i in keys:
    outputFile.write(i + "\n")
    sa = reg[i]
    sa.sort()
    for t in sa:
        outputFile.write(t + "\n")
outputFile.close()


