class king_lion():
    def __init__(self):
        self._x = 47
    
    def get_x (self):
        return self._x
    
    def del_x (self):
        self._x = 47
    
    def set_x(self, value):
        if type(value) != int:
            return
        if (value >= -100) and (value <= 100):
            self._x = value
        else:
            self._x = 0

    x = property(get_x, set_x, del_x)
