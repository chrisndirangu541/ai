from Main import Student

try:
    s = Student("ok")
    print("suc")
except:
    print("fail")

try:
    s = Student("o k")
    print("suc")
except:
    print("fail")

try:
    s = Student(" o k")
    print("suc")
except:
    print("fail")

try:
    s = Student("oK")
    print("suc")
except:
    print("fail")

try:
    s = Student("oK2")
    print("fail")
except:
    print("suc")

try:
    s = Student()
    print("suc")
except:
    print("fail")

try:
    s = Student()
    s.name = "ok"
    е = s.name
    print("suc")
except:
    print("fail")

try:
    s = Student()
    s.name = "привет"
    print("fail")
except:
    print("suc")

