from string import ascii_letters

class Student:
    def __init__(self, name=''):
        self.name = name

    @property
    def name(self):
        return self.__x

    @name.setter
    def name(self, x):
        if x == '':
            self.__x = x
            return
        if type(x) != str:
            raise ValueError

        for i in x:
            if i not in ascii_letters and i != " ":
                raise ValueError
        self.__x = x