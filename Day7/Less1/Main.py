import time

def time_decorator(decorated_func):
    def dec(n):
        s_t = time.time() * 1000
        print("Start time: " + str(s_t))
        decorated_func(n)
        e_t = time.time() * 1000
        print("End time: " + str(e_t))
        print("Execution time time: " + str(e_t - s_t))
    return dec

def counter_decorator(decorated_func):
    i = 0
    def dec(a, b):
        nonlocal i
        i += 1
        print("Function calls count: " + str(i))
        decorated_func(a, b)
    return dec


def method_decorator(decorated_method):

    def dec(self, x, y, z):
        decorated_method(self, 3 * x - 7 * y + 15 * z + 18)

    return dec
