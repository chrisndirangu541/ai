import numpy

class GradientOptimizer:
    def __init__(self, oracle, x0):
        self.oracle = oracle
        self.x0 = x0

    def optimize(self, iterations, eps, alpha):
        x_last = self.x0
        for i in range(iterations):
            x_new = x_last - alpha * self.oracle.get_grad(x_last)
            if(numpy.linalg.norm((x_new - x_last), ord=2) < 0.3 * eps):
                return x_new
            x_last = x_new
        return x_last

class Oracle:
    def get_func(self, x):
        pass
    def get_grad(self, x):
        pass