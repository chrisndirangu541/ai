import lineRegress1 as LR1
import numpy as np

test1 = np.array([1,2,3,4], dtype='float')
test2 = np.array([1,1,1,2], dtype='float')

t = LR1.linear_func(test1, test2)
print(t)

test3 = np.array([1,2,1,1], dtype='float')
test4 = np.array([[1,2,3,4],[2,2,2,2]], dtype='float')

t = LR1.linear_func_all(test3, test4)
print(t)

test5  = np.array([30,15], dtype='float')
LR1.mean_squared_error(test3, test4, test5)

t = LR1.grad_mean_squared_error(test3, test4, test5)

t - LR1.fit_linear_regression(test4, test5)
print(t)