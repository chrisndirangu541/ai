import numpy as np
import sys

def GetData(valueType):
    inputData = input().split(" ")
    result = list(map(valueType, filter(lambda x: x!="", inputData)))
    return result


sizes = GetData(int)
mat1 = np.array(GetData(int))
mat2 = np.array(GetData(int))

mat = mat1 + mat2

lmat = mat.tolist()
lmat = list(map(str, lmat))
print(" ".join(lmat))