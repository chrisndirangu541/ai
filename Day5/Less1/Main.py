import numpy as np

inputData = input()
splitedData = inputData.split(" ")
clearData = list(map(int, filter(lambda x: x != "", splitedData)))
vector = np.array(clearData)

print(int(np.linalg.norm(vector, ord=1)))
