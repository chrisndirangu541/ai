import numpy as np

def GetData(valueType):
    inputData = input().split(" ")
    result = list(map(valueType, filter(lambda x: x!="", inputData)))
    return result


sizes = GetData(int)
mat = np.array(GetData(int)[:sizes[0]])
ids = np.array(GetData(int)[:sizes[1]])

ids -= 1
mat[ids] += 1

lmat = mat.tolist()
lmat = list(map(str, lmat))
print(" ".join(lmat))

