import numpy as np
import pandas as pd
import sklearn.model_selection as sk_ms
import sklearn.ensemble as sk_en
from sklearn.preprocessing import Normalizer
from sklearn.preprocessing import PolynomialFeatures
from sklearn.svm import LinearSVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neighbors import RadiusNeighborsClassifier

SEED = 900990

data = pd.read_csv("wine_X_train.csv")
del data["Unnamed: 0"]

X_train = data.iloc[:, :-1]
y_train = data.iloc[:, -1]

y_train = y_train.map(lambda x: 0 if x <= 6.0 else 1)

normalizer = Normalizer(norm='l2')
polynomialer = PolynomialFeatures(degree = 4)


X_train, X_test, y_train, y_test = sk_ms.train_test_split(X_train, y_train, random_state = SEED )


#X_test = pd.read_csv("wine_X_test.csv")
#del X_test["Unnamed: 0"]

X_concat = pd.concat([X_train, X_test])

polynomialer.fit(X_concat)
normalizer.fit(X_concat)

X_train = polynomialer.transform(X_train)
X_train = normalizer.transform(X_train)

X_test = polynomialer.transform(X_test)
X_test = normalizer.transform(X_test)


selector = LinearSVC(penalty='l1', dual=False, random_state = SEED, max_iter=3000)
selector.fit(X_train, y_train)

clSVC = LinearSVC(penalty='l1', dual=False, random_state = SEED, max_iter=3000)
classificator.fit(X_train, y_train)
print("SVC : %f" %classificator.score(X_test, y_test))

classificator = sk_en.RandomForestClassifier(random_state = SEED)
classificator.fit(X_train, y_train)
print("Forest : %f" % classificator.score(X_test, y_test))

classificator = sk_en.GradientBoostingClassifier(random_state = SEED)
classificator.fit(X_train, y_train)
print("GB : %f" % classificator.score(X_test, y_test))

classificator = sk_en.AdaBoostClassifier(random_state = SEED)
classificator.fit(X_train, y_train)
print("Ada : %f" % classificator.score(X_test, y_test))

print("Select features")
i = 0
selectedFeatures = []
for coef in selector.coef_[0]:
    if coef != 0:
        selectedFeatures.append(i)
    i += 1
X_train = X_train[:, selectedFeatures]
X_test = X_test[:, selectedFeatures]

classificator = KNeighborsClassifier(n_jobs = -1)
classificator.fit(X_train, y_train)
print("KN : %f" % classificator.score(X_test, y_test))

classificator = RadiusNeighborsClassifier(n_jobs = -1)
classificator.fit(X_train, y_train)
print("RN : %f" % classificator.score(X_test, y_test))

classificator = sk_en.ExtraTreesClassifier(random_state = SEED, n_jobs = -1)
classificator.fit(X_train, y_train)
print("EX : %f" % classificator.score(X_test, y_test))

